package cap_4;

public class ConditionalOperator {
	public static void main(String [] args) {	
		int sizeOfYard = 10;
		int numOfPets  = 5;
		String status =  (numOfPets < 4) ? "Pet count ok" : (sizeOfYard > 8) ? "Pet limit on edge" : "too many pets";
		System.out.println("Pet status is " + status);
		// for numOfPets  = 3  prints Pet status is Pet count ok
		// for numOfPets  = 5  prints Pet status is Pet limit on edge
		Integer i = 42;
		String s  = (i<40) ? "life" : "no";
		System.out.println(s);
	}

}
