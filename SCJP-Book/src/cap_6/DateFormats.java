package cap_6;

import java.io.File;
import java.text.DateFormat;
import java.util.*;
import java.util.Date;

public class DateFormats {
	public static void main(String [] args) {
		Calendar c = Calendar.getInstance();
		c.set(2010, 11, 14);// December 14, 2010
		
		Date d2 = c.getTime();
		
		
		Locale locIN = new Locale("hi", "IN"); // India
		Locale locJA = new Locale("ja"); //Japan
		
		DateFormat dfUS = DateFormat.getInstance();
		System.out.println("US  " + dfUS.format(d2)); // US  12/14/10, 1:18 PM
		
		DateFormat dfUSfull = DateFormat.getDateInstance(DateFormat.FULL);
		System.out.println("US full  " + dfUSfull.format(d2)); //US full  Tuesday, December 14, 2010
		
		Locale locIT = new Locale("it", "IT"); //Italy
		DateFormat dfIT = DateFormat.getDateInstance(DateFormat.FULL, locIT);
		System.out.println("Italy " + dfIT.format(d2)); // Italy marted� 14 dicembre 2010
		
		Locale locPT = new Locale("pt");  //Portugal
		DateFormat dfPT = DateFormat.getDateInstance(DateFormat.FULL, locPT);
		System.out.println("Portugal " + dfPT.format(d2)); // Portugal ter�a-feira, 14 de dezembro de 2010
		
		Locale locBR = new Locale("pt", "BR"); //Brazil
		System.out.println("def " + locBR.getDisplayCountry());
		// in Brazil the country is called Brasil
		//output: loc Brasil
		System.out.println("loc " + locBR.getDisplayCountry(locBR)); 
		
		Locale locDK = new Locale("da", "DK"); // Danmark
		//In US the default for Danish language is danish
		// output: def Danish
		System.out.println("def "+ locDK.getDisplayLanguage());
		//in Denmark the language is called dansk
		// output: Loc dansk
		System.out.println("Loc " + locDK.getDisplayLanguage(locDK));
		
		StringBuffer sb1 = new StringBuffer("abc");
		StringBuffer sb2 = sb1;
		sb1.append("d");
		System.out.println(sb1 +" "+ sb2 + (sb1 == sb2));
	}

}
