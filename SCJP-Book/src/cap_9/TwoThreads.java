package cap_9;

public class TwoThreads {
	static Thread lauren, hardy;
	public static void main(String[] args) {
		lauren = new Thread() {
			public void run() {
				System.out.println("A");
				try {
					hardy.sleep(1000);
				} catch (Exception e) {
					System.out.println("B");
				}
				System.out.println("C");
			}
		};
		
		hardy = new Thread() {
			public void run() {
				System.out.println("d");
				try {
					lauren.wait();
				} catch (Exception e) {
					System.out.println("e");
				}
				System.out.println("f");
			}
		};
		lauren.start();
		hardy.start();
	}
}
