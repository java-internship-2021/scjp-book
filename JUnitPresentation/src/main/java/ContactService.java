import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ContactService {
    List<Contact> contactList = new ArrayList<Contact>();

    public void addContact(String firstName, String lastName, String phoneNumber) {
        Contact contact = new Contact(firstName, lastName, phoneNumber);
        validateContact(contact);
        checkIfContactAlreadyExist(contact);
        contactList.add(contact);
    }

    public Collection<Contact> getAllContacts() {
        return contactList;
    }

    private void checkIfContactAlreadyExist(Contact contact) {
        if (contactList.contains(contact))
            throw new RuntimeException("Contact Already Exists");
    }

    private void validateContact(Contact contact) {
        contact.validateFirstName();
        contact.validateLastName();
        contact.validatePhoneNumber();
    }


}
