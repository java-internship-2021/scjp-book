import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;


import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ContactServiceTest {

    private ContactService contactService ;

    @BeforeAll
    public static void setUpAll(){
        System.out.println("Should print before all tests \n");
    }
    @BeforeEach
    public void setUp(){
        System.out.println ("Instantiating contact service");
        contactService = new ContactService();
    }

    @Test
//    @DisplayName("Should create contact")
    public void shouldCreateContact() {
        contactService.addContact("John", "Doe", "0123456789");
        assertFalse(contactService.getAllContacts().isEmpty());
        assertEquals(1, contactService.getAllContacts().size());
    }

    @Test
    @DisplayName("Should not create contact when first name is null")
    public void shouldThrowRuntimeExceptionWhenFirstNameIsNull() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            contactService.addContact(null, "Doe", "0123456789");
        });
    }

    @Test
    @DisplayName("Should not create contact when last name is null")
    public void shouldThrowRuntimeExceptionWhenLastNameIsNull() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            contactService.addContact("John", null, "0123456789");
        });
    }

    @Test
    @DisplayName("Should not create contact when phone number is null")
    @Disabled
    public void shouldThrowRuntimeExceptionWhenPhoneNumberIsNull() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            contactService.addContact("John", "Doe", null);
        });
    }

    /*
    * CONDITIONAL EXECUTIONS
    * */

    @Test
    @DisplayName("Should create contact on MAC")
    @EnabledOnOs(value = OS.MAC, disabledReason = "Should Run only on MAC")
    public void shouldCreateContactCondition() {
        contactService.addContact("John", "Doe", "0123456789");
        assertFalse(contactService.getAllContacts().isEmpty());
        assertEquals(1, contactService.getAllContacts().size());
    }

    @Test
    @DisplayName("Should create contact on Windows")
    @EnabledOnOs(value = OS.WINDOWS, disabledReason = "Should Run only on Windows")
    public void shouldCreateContactConditionWin() {
        contactService.addContact("John", "Doe", "0123456789");
        assertFalse(contactService.getAllContacts().isEmpty());
        assertEquals(1, contactService.getAllContacts().size());
    }

    // ASSUMPTIONS

    @Test
    @DisplayName("Test contact creation on developer machine")

    public void shouldTestContactCreationOnDEV() {
        Assumptions.assumeTrue("DEV".equals(System.getProperty("ENV")));
        contactService.addContact("John", "Doe", "0123456789");
        assertFalse(contactService.getAllContacts().isEmpty());
        assertEquals(1, contactService.getAllContacts().size());
    }

    //REPEATED TESTS

    @DisplayName("Repeat contact creation test 5 times")
    @RepeatedTest(5)
    public void shouldTestContactCreationRepeatedly() {
        contactService.addContact("John", "Doe", "0123456789");
        assertFalse(contactService.getAllContacts().isEmpty());
        assertEquals(1, contactService.getAllContacts().size());
    }

    @DisplayName("Method source case - Phone number should match the required format")
    @ParameterizedTest
    @MethodSource("phoneNumberList")
    public void shouldTestPhoneNumberFormatUsingMethodSource(String phoneNumber) {
        contactService.addContact("John", "Doe", phoneNumber);
        assertFalse(contactService.getAllContacts().isEmpty());
        assertEquals(1, contactService.getAllContacts().size());
    }

    private List<String> phoneNumberList() {
        return Arrays.asList("0123456789", "1234567890", "+0123456789");
    }

    @Nested
    class ParamTest {
        //PARAMETERIZED TESTS

        @DisplayName("Phone number should match the required format")
        @ParameterizedTest
        @ValueSource(strings = {"0123456789", "1234567890", "+0123456789"})
        public void shouldTestPhoneNumberFormat(String phoneNumber) {
            contactService.addContact("John", "Doe", phoneNumber);
            assertFalse(contactService.getAllContacts().isEmpty());
            assertEquals(1, contactService.getAllContacts().size());
        }

        @DisplayName("CSV File Source Case - Phone number should match the required format")
        @ParameterizedTest
        @CsvFileSource(resources = "/data.csv")
        public void shouldTestPhoneNumberFormatUsingCSVFileSource(String phoneNumber) {
            contactService.addContact("John", "Doe", phoneNumber);
            assertFalse(contactService.getAllContacts().isEmpty());
            assertEquals(1, contactService.getAllContacts().size());
        }

    }

    @AfterEach
    public void tearDown() {
        System.out.println("Should execute after each test\n");
    }

    @AfterAll
    public static void tearDownAll() {
        System.out.println("Should be executed at the end of the tests");
    }
}
