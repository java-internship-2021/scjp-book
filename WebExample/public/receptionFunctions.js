

$( ".letter" ).click(function() {
    var clickedValue = $(this).text();
    var  filter, table, tr, td, i, txtValue;
    filter = clickedValue.toUpperCase();
    table = document.getElementById("receptionTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }}
    } 
});