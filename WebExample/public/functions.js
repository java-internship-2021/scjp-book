$(document).ready(function() {
    $('#userInput').on('input change', function() {
        if($(this).val() != '' ) {
            $('#passwordInput').on('input change', function() {
                if($(this).val() != '' ) {
                    $('#authBtn').prop('disabled', false);
                }
            })}
        else {
            $('#authBtn').prop('disabled', true);
        }
    });
});


$("#inputImage").change(function(e) {

    for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
        
        var file = e.originalEvent.srcElement.files[i];
        
        var img = document.createElement("img");
        var reader = new FileReader();
        reader.onloadend = function() {
             img.src = reader.result;
        }
        reader.readAsDataURL(file);
        $("#inputImage").after(img);
    }
});




// let goptions = document.getElementById("gestiuneOptions");
// goptions.addEventListener("change", enableFunctionOptions);

// function enableFunctionOptions(){
//     if(document.getElementById("functieOptions").value != "Selecteaza o gestiune:"){
//         goptions.disabled = false;
//     }
// };

