package com.example.mockito2;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.BDDMockito.*;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;




@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.Silent.class) 
class BookValidatorTests {
	
	@Mock
	Book book ;
	
	@Test
	void testGettersSetters() {

		book.setId(1);
		book.setName("Harry Potter");
		book.setAuthor("J.K.Rowling");
		book.setPrice(100);
		verify(book).setId(ArgumentMatchers.eq(1));
		verify(book).setName(ArgumentMatchers.eq("Harry Potter"));
		verify(book).setAuthor(ArgumentMatchers.eq("J.K.Rowling"));
		verify(book).setPrice(ArgumentMatchers.eq(100));
	}
	
	@Test
	void testGettersSetters1() {
		
		when(book.getId()).thenReturn(1);
		assertEquals(1, book.getId());
		
		when(book.getName()).thenReturn("Harry Potter");
		assertEquals("Harry Potter", book.getName());
		
		when(book.getAuthor()).thenReturn("J.K.Rowling");
		assertEquals("J.K.Rowling", book.getAuthor());
		
		when(book.getPrice()).thenReturn(100);
		assertEquals(100, book.getPrice());

	}
	
	@Mock
	Book bookId;
	
	
	@InjectMocks
	Validator validator;
	
	
	@Test
	public void testValidatorId() throws Exception{
	
		lenient().when(bookId.getId()).thenReturn(-234);
		
		Throwable exception = assertThrows(Exception.class, ()->validator.validate());
		assertEquals("Id should be a positive integer", exception.getMessage());

//		try {
//			validator.validate();
//		} catch (Exception e) {
//			// TODO: handle exception
//			assertEquals("Id should be a positive integer", e.getMessage());
//		}
	}
	
	@Test
	public void testValidatorName() throws Exception{
		
		
		when(book.getId()).thenReturn(1);
		lenient().when(bookId.getName()).thenReturn("");
		
		Throwable exception = assertThrows(Exception.class, ()->validator.validate());
		assertEquals("Name should not be empty", exception.getMessage());

	}
	
	@Test
	public void testValidatorAuthor() throws Exception{
		
		
		when(book.getId()).thenReturn(1);
		when(book.getName()).thenReturn("Harry Potter");
		lenient().when(bookId.getAuthor()).thenReturn(null);
		
		Throwable exception = assertThrows(Exception.class, ()->validator.validate());
		assertEquals("Author name should not be empty", exception.getMessage());

	}
	
	@Test
	public void testValidatorPrice() throws Exception{
		
		
		when(book.getId()).thenReturn(1);
		when(book.getName()).thenReturn("Harry Potter");
		when(book.getAuthor()).thenReturn("J.K.Rowling");
		
		lenient().when(bookId.getPrice()).thenReturn(-3847);
		
		Throwable exception = assertThrows(Exception.class, ()->validator.validate());
		assertEquals("Price should be a positive integer", exception.getMessage());

	}

}
