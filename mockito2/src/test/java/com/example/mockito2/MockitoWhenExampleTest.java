package com.example.mockito2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.Spy;

@ExtendWith(MockitoExtension.class)
public class MockitoWhenExampleTest {
//	@Mock
//	Iterator<String> i;
//	
//	Comparable<String> c; 
	
	@Spy
	List<String> spy = new LinkedList<>();
	
    @Test
    public void testLinkedListSpyCorrect() {
    	spy.add("foo");

        // when(spy.get(0)).thenReturn("foo");
        // would not work as the delegate it called so spy.get(0)
        // throws IndexOutOfBoundsException (list is still empty)

        // you have to use doReturn() for stubbing
        doReturn("foo").when(spy).get(0);

        assertEquals("foo", spy.get(0));
    }
	
//	@Test
//	public void testMoreThanOneCalue() {
//		when(i.next()).thenReturn("Mockito").thenReturn("rocks");
//		String result = i.next()+ " "+ i.next();
//		assertEquals("Mockito rocks", result);
//	}

}
