package com.example.mockito2;

public class Book {

	private Integer id;
	private String name;
	private String author;
	private Integer price;
	
	public Book() {}
	public Book(Integer newid, String name, String author, Integer price) {
		this.id = newid;
		this.name = name;
		this.author = author;
		this.price = price;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public int computeSale(int percent) {
		setPrice(price - percent);
		//int result = getPrice();
		return price;
	}

}
