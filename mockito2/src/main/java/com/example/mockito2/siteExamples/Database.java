package com.example.mockito2.siteExamples;

public class Database {
	private int id;
    public boolean isAvailable() {
        // currently not implemented, as this is just demo used in a software test
        return false;
    }
    public int getUniqueId() {
        return 42;
    }
    public void setUniqueId(int nr) {
    	this.id = nr;
    }

}
