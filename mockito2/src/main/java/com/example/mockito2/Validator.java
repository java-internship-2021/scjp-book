package com.example.mockito2;
public class Validator {
	private Book book;
	public Validator(Book b) {
		// TODO Auto-generated constructor stub
		this.book = b;
	}
	public void validate() throws Exception {
		if (book.getId() <= 0) {	
			throw new Exception("Id should be a positive integer");
		}
		if (book.getName()== null || book.getName()== "") {
			throw new Exception("Name should not be empty");
		}
		if (book.getAuthor()== null || book.getAuthor() == "") {
			throw new Exception("Author name should not be empty");
		}
		if (book.getPrice() <= 0) {
			throw new Exception("Price should be a positive integer");
		}
	}

}
