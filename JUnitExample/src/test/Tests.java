package test;
import static org.junit.Assert.*;
import org.junit.Test;

import main.Book;
import main.Validator;

public class Tests {

	@Test
	//TEST GETTERS AND SETTERS
	public void testGettersSetters() {
		Book b = new Book();
		Integer idTest = 1;
		String nameTest = "Harry Potter";
		String authorTest = "J.K.Rowling";
		Integer priceTest = 123;
		
		
		b.setId(idTest);
		assertEquals(idTest, b.getId());
		
		b.setName(nameTest);
		assertEquals(nameTest, b.getName());
		
		b.setAuthor(authorTest);
		assertEquals(authorTest, b.getAuthor());
		
		b.setPrice(priceTest);
		assertEquals(priceTest, b.getPrice());
		assertTrue(true);
	}
	
	@Test
	public void testConstructor()
	{
		Book book = new Book(1,"Harry Potter", "J.K.Rowling", 123 );
		assertEquals((Integer)1, book.getId());
		assertEquals("Harry Potter", book.getName());
		assertEquals("J.K.Rolling", book.getAuthor());
		assertEquals((Integer)123, book.getPrice());
		
	}
	
	
	@Test
	public void testValidatorId() {
		Book book = new Book(-1,"Harry Potter", "J.K.Rowling", 123 );
		Validator validator = new Validator();
		try {
			validator.validate(book);
		} catch (Exception e) {
			// TODO: handle exception
			assertEquals("Id should be a positive integer", e.getMessage());
		}
	}
	@Test
	public void testValidatorName() {
		Book book = new Book(1,"", "J.K.Rowling", 123 );
		Validator validator = new Validator();
		try {
			validator.validate(book);
		} catch (Exception e) {
			// TODO: handle exception
			assertEquals("Name should not be empty", e.getMessage());
		}
	}
	
	@Test
	public void testValidatorAuthor() {
		Book book = new Book(1,"H", null, 123 );
		
		Validator validator = new Validator();
		try {
			validator.validate(book);
		} catch (Exception e) {
			// TODO: handle exception
			assertEquals("Author name should not be empty", e.getMessage());
		}
	}

	@Test
	public void testValidatorPrice() {
		Book book = new Book(1,"Harry Potter", "J.K.Rowling", -67 );
		Validator validator = new Validator();
//		try {
//			validator.validate(book);
//		} catch (Exception e) {
//			// TODO: handle exception
//			assertEquals("Price should be a positive integer", e.getMessage());
//		}
		assertThrows(Exception.class, () -> validator.validate(book));
	}
	
	@Test
	public void testComputeSale() {
		Book book = new Book(1,"Harry Potter", "J.K.Rowling", 100 );
		int priceAfterSale = book.computeSale(0);
		assertEquals(100, priceAfterSale);
		
		int priceAfterSale1 = book.computeSale(20);
		assertEquals(80, priceAfterSale1);
	}
	
}
