package main;
public class Validator {
	public void validate(Book book) throws Exception {
		if (book.getId() <= 0) {	
			throw new Exception("Id should be a positive integer");
		}
		if (book.getName()== null || book.getName()== "") {
			throw new Exception("Name should not be empty");
		}
		if (book.getAuthor()== null || book.getAuthor() == "") {
			throw new Exception("Author name should not be empty");
		}
		if (book.getPrice() <= 0) {
			throw new Exception("Price should be a positive integer");
		}
	}

}
